# castle-map

## Development Setup

Perform the following steps to set up the development environment:

1. Run `yarn install` to install the project's dependencies
2. Run `yarn start` to start a development server on [http://localhost:3000](http://localhost:3000).

## Production Build

Run the command `yarn build` to create a production build of the project in the `build` folder.

## Deployment via Docker

1. Build the Docker image: `docker build . -t castle-map`
2. Start a Docker container: `docker run --rm -p 3000:80 castle-map`
3. Open the website at [http://localhost:3000](http://localhost:3000).
